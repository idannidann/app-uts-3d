package azra.wildan.apputs3d

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.android.synthetic.main.activity_login.*
import java.lang.Exception

class LoginActivity : AppCompatActivity(), View.OnClickListener {
    var bundle :Bundle? = null
    var topik = "apputs3d"
    var type = 0

    override fun onClick(v: View?){
        when(v?.id){
            R.id.btnLogin ->{
                var email = edUserName.text.toString()
                var password = edPassword.text.toString()

                if(email.isEmpty() || password.isEmpty()) {
                    Toast.makeText(this, "username / password can't be empty",Toast.LENGTH_LONG).show()
                }
                else{
                    val progressDialog = ProgressDialog(this)
                    progressDialog.isIndeterminate = true
                    progressDialog. setMessage("Authenticating...")
                    progressDialog.show()

                    FirebaseAuth.getInstance().signInWithEmailAndPassword(email,password)
                        .addOnCompleteListener{
                            progressDialog.hide()
                            if(!it.isSuccessful) return@addOnCompleteListener
                            Toast.makeText(this, "successfully Login", Toast.LENGTH_SHORT).show()
                            val intent = Intent (this, MainActivity::class.java)
                            startActivity(intent)
                        }
                        .addOnFailureListener{
                            progressDialog.hide()
                            Toast.makeText(this,
                                "Username/Password incorrect", Toast.LENGTH_SHORT).show()
                        }
                }
            }
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        btnLogin.setOnClickListener(this)

        FirebaseMessaging.getInstance().subscribeToTopic(topik)
    }
    override fun onResume() {
        super.onResume()

        FirebaseInstanceId.getInstance().instanceId.addOnCompleteListener(
            OnCompleteListener { task ->
                if (!task.isSuccessful) return@OnCompleteListener
                txToken.setText(task.result!!.token)
            }
        )
        try {
            bundle = getIntent().getExtras()!!
        }catch (e: Exception){
            Log.e("BUNDLE", "bundle is null")
        }

        if (bundle != null){
            type = bundle!!.getInt("type")
            when(type){
                1 -> {
                    txTitle.setText(bundle!!.getString("title"))
                    txDesc.setText(bundle!!.getString("body"))
                }
            }
        }
    }
}